# Dropwizard Greet World Demo

## About

This demo goes a little further than a simple "Hello World". It touches various topics when
developing REST services like database access, authentication, authorization, scheduled tasks, etc.
Most of these topics has been implemented in the most simple way to get you started.

The application can be run on a [Kubernetes](https://kubernetes.io/) cluster or 
[minikube}(https://github.com/kubernetes/minikube) but it can also run locally as a normal java 
application on your desktop or server.


## How to start the application locally 

1. Run `mvn clean package` to build your application
2. Start application with `java -jar target/greetworld-1.0.0.jar server src/main/resources/config.json`
3. To check that your application is running enter url `http://localhost:8080/greeting/en`

There are various users configured. The user _george_ with the secret _egroeg_ has the right 
permission to access this resource. The user _me_ with the secret _em_ hasn&apos;t. 


## How to deploy the application on minikube

First you have to start your minikube cluster (`minikube start`). Then use `mvn fabric8:run` to
deploy this application on your cluster.


## How to test the application on minikube

You need to retrieve the IP address of your cluster.

    minikube ip

Then you need to identify the port which has been assigned to your REST service.

    kubectl get services


## Features used in the Demo

### Database

H2 has been used as an in-memory database.

#### Database Credentials

In this case the database can be accessed without any credentials because this is a demo. In most
production cases you will need some credentials to access the database. These credentials can be 
added as a _configmap_ to the Kubernetes cluster and then setup as environment variables, see
[here](https://kubernetes.io/docs/tasks/inject-data-application/distribute-credentials-secure/#create-a-pod-that-has-access-to-the-secret-data-through-environment-variables).

#### Database Setup

For this demo it is convenient to have a database ready and filled with data on application start.
[Flyway](https://flywaydb.org/) is used for setting up the database and adding some data to it. I 
don&apos;t use the Dropwizard module in this case because the Dropwizard module registers commands
for working with Flyway which is not my use case here. I just need to execute _clean_ and _migrate_.

The migration scripts can be found at _src/main/resources/db/migrations_.

#### Database Access

The database is accessed via JDBI which is integrated in Dropwizard and only needs to add the
_dropwizard-jdbi_ maven dependency.

Dao and mapper classes can be found in the _greetworld.dao_ package.

### Scheduled Tasks

Some things need to happen at a certain time or in certain intervals. This can be achieved with the 
dropwizard bundle _dropwizard-jobs-core_. Jobs can be configured with the CRON syntax.

This application uses a job to update the cache with the greetings from the database, see
_GreetingsCacheUpdater_.

### Dependency Injection

As the dependency tree for objects becomes more complicated it is easier to let a DI framework take
the part of setting the objects up. Guice is such a framework which integrates nicely and doesn't
spread through your whole code base.

The only thing that one has to consider when using Guice is that some things cannot be used during
_initialze()_:

> If you are having trouble accessing your Configuration or Environment inside a Guice Module, you could try using a provider.

Using a provider is a nice workaround for that, see on the Guice website _"Environment and Configuration"_.

The whole binding stuff can be found in the _ServiceBinder_ class.

### Authentication

Dropwizard supports authentication out-of-the-box which is great. For the demo I have settled with
Basic Auth.

The Dropwizard User Manual section about authentication explains very well how to set it up.

This demo application loads the user names and secrets in the class _PropertiesFileUserProfileLoader_ 
from the properties file

    src/main/resources/users.properties

The secret is the reversed user name.


### Authorization

Dropwizard also supports authorization out-of-the-box. It is explained in the _Authentication_ section, 
_Protecting Resources_.

So all in all you will need something like this in your code:

    // setup auth
    env.jersey().register(guice.getInjector().getInstance(AuthDynamicFeature.class));
    env.jersey().register(RolesAllowedDynamicFeature.class);
    env.jersey().register(new AuthValueFactoryProvider.Binder<>(User.class));

The roles are loaded from

    src/main/resoures/roles.properties

by the class _PropertiesFileUserProfileLoader_.


## Fabric8

Most of the time you need to make some configuration adjustments. The easiest way to do this is via
[Resource Fragments](https://maven.fabric8.io/#resource-fragments). These fragments are located at

    src/main/fabric8

and don't represent the full configuration of the entity but only the parts which should take
precedence over the default values.

### Dropwizard Configuration

The Dropwizard configuration needs to be stored somewhere. A Kubernetes ConfigMap is one way to go.
In this demo application the configuration is stored in

    src/main/fabric8/rest-configmap.yml

In the ''deployment.yml'' the configuration is added as a volume in the _volumes_ section.
The JSON objects in the _data_ JSON object are mounted as files at ''/etc/greetworld'' in the docker 
instance with their JSON values as content of the files, see _volumeMounts_ in deployment.yml.

### REST Service

The port for the REST service needs to be specified, see _rest-service.yml_.

### Starting Dropwizard Application

Dropwizard needs at least a _command_ (in our case ''server'') and probably a configuration file for
its configuration as arguments. Normally we would start the application like

    java -jar greetworld-1.0.0.jar server /path/to/config.yml

The [used docker image](https://github.com/fabric8io-images/java) comes with a start script for the
java application, ''/deployments/run-java.sh''. This can be used as a command for the docker image,
see _command_ section in deployment.yml.

The arguments, ''server'' and config file, need to be added to the ''args'' section in deployment.yml.

For example:

    containers:
      - command : [ "/deployments/run-java.sh" ]
        args : [ "server", "/etc/greetworld/config.json" ]

The path to the config file needs to correspond with the mounted configmap, see _volumeMounts_.

### Build

The build process in Fabric8 is implemented with the help of Jenkins Pipelines. For this you need a 
Jenkinsfile which defines the build steps.

The github repository [fabric8-jenkinsfile-library](https://github.com/fabric8io/fabric8-jenkinsfile-library) 
contains many example Jenkinsfile files.


## Links
* [Fabric8](https://fabric8.io)
* [Dropwizard with Fabric8 and Kubernetes](http://wiki.rpgnextgen.com/doku.php?id=dropwizard_with_fabric8_and_kubernetes)
* [H2](http://h2database.com)
* [Dropwizard Jobs](https://github.com/spinscale/dropwizard-jobs)
* [Dropwizard Guice](https://github.com/HubSpot/dropwizard-guice)
* [YAML Syntax](https://en.wikipedia.org/wiki/YAML)
* [YAMLlint](http://www.yamllint.com/)
* [Dropwizard with Fabric8 and K8s](http://wiki.rpgnextgen.com/doku.php?id=dropwizard_with_fabric8_and_kubernetes)
