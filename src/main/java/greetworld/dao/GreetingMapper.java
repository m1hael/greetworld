package greetworld.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import greetworld.data.Greeting;

public class GreetingMapper implements ResultSetMapper<Greeting> {

	@Override
	public Greeting map(int index, ResultSet r, StatementContext ctx) throws SQLException {
		Greeting g = new Greeting(r.getString("text"), r.getString("country"));
		return g;
	}

}
