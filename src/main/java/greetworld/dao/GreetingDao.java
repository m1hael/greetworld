package greetworld.dao;

import java.util.List;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;

import greetworld.data.Greeting;

public interface GreetingDao {

	@SqlQuery("SELECT * FROM greetworld.greetings WHERE country = :country")
	Greeting get(@Bind("country") String country);
	
	@SqlQuery("SELECT * FROM greetworld.greetings")
	List<Greeting> list();
	
}
