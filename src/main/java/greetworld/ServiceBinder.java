package greetworld;

import org.flywaydb.core.Flyway;
import org.skife.jdbi.v2.DBI;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.jdbi.InstrumentedTimingCollector;
import com.google.common.cache.CacheBuilderSpec;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.TypeLiteral;

import greetworld.auth.SimplePermissionAuthorizer;
import greetworld.auth.UserAuthenticator;
import greetworld.dao.GreetingDao;
import greetworld.dao.GreetingMapper;
import greetworld.data.User;
import greetworld.job.GreetingsCacheUpdater;
import greetworld.rs.GreetingResource;
import greetworld.service.GreetingService;
import io.dropwizard.auth.AuthDynamicFeature;
import io.dropwizard.auth.Authenticator;
import io.dropwizard.auth.Authorizer;
import io.dropwizard.auth.CachingAuthenticator;
import io.dropwizard.auth.basic.BasicCredentialAuthFilter;
import io.dropwizard.auth.basic.BasicCredentials;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.setup.Environment;

public class ServiceBinder extends AbstractModule {

	private MetricRegistry metricRegistry;

	public ServiceBinder(MetricRegistry metricRegistry) {
		this.metricRegistry = metricRegistry;
	}

	@Override
	protected void configure() {
		bind(GreetingsCache.class).in(Singleton.class);
		bind(GreetingsCacheUpdater.class).in(Singleton.class);
		bind(GreetingResource.class);
		bind(GreetingService.class);
		bind(new TypeLiteral<Authorizer<User>>() {}).to(SimplePermissionAuthorizer.class);
		bind(UserAuthenticator.class);
	}

	@Provides
	public DBI provideDbi(Environment env, ApplicationConfiguration configuration, GreetingMapper mapper) {
		final DBIFactory factory = new DBIFactory();
		
		// Here we could configure the credentials for the database access but is for in-memory db not needed
		//
		// DataSourceFactory dataSourceFactory = configuration.getDataSourceFactory();
		// dataSourceFactory.setUser(System.getenv("GREETWORLD_DB_USERNAME"));
		// dataSourceFactory.setPassword(System.getenv("GREETWORLD_DB_PASSWORD"));
		
		final DBI jdbi = factory.build(env, configuration.getDataSourceFactory(), "h2pool");
		jdbi.setTimingCollector(new InstrumentedTimingCollector(metricRegistry));
		jdbi.registerMapper(mapper);
		return jdbi;
	}

	@Provides
	public GreetingMapper getGreetingMapper() {
		return new GreetingMapper();
	}

	@Provides
	public GreetingDao getGreetingDao(DBI jdbi) {
		return jdbi.onDemand(GreetingDao.class);
	}
	
	@Provides
	public Flyway getFlyway(ApplicationConfiguration configuration) {
		Flyway flyway = new Flyway();
		
		DataSourceFactory dataSourceFactory = configuration.getDataSourceFactory();
		dataSourceFactory.setUser(System.getenv("GREETWORLD_DB_USERNAME"));
		dataSourceFactory.setPassword(System.getenv("GREETWORLD_DB_PASSWORD"));
		
		flyway.setDataSource(dataSourceFactory.build(metricRegistry, "Flyway Datasource"));
		flyway.setLocations("classpath:/db/migrations");
		flyway.setSchemas("greetworld");
		flyway.setSqlMigrationSeparator("-");
		
		return flyway;
	}
	
	@Provides
	public Authenticator<BasicCredentials, User> providerAuthenticator(ApplicationConfiguration configuration, UserAuthenticator authenticator) {
		CacheBuilderSpec cacheSpec = CacheBuilderSpec.parse("maximumSize=10000,expireAfterAccess=10m");
		CachingAuthenticator<BasicCredentials, User> cachingAuthenticator = new CachingAuthenticator<>(
				metricRegistry, authenticator, cacheSpec);
		return cachingAuthenticator;
	}
	
	@Provides
	public AuthDynamicFeature provideAuthFeature(Authenticator<BasicCredentials, User> authenticator, Authorizer<User> authorizer) {
		AuthDynamicFeature feature = new AuthDynamicFeature(
	            new BasicCredentialAuthFilter.Builder<User>()
	                .setAuthenticator(authenticator)
	                .setAuthorizer(authorizer)
	                .setRealm("GreetWorld")
	                .buildAuthFilter());
		
		return feature;
	}
}
