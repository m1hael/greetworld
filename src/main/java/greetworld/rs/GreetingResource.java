package greetworld.rs;

import javax.annotation.security.DenyAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;

import greetworld.data.Greeting;
import greetworld.data.User;
import greetworld.service.GreetingService;
import io.dropwizard.auth.Auth;

@DenyAll
@Path("/greeting")
@Produces(MediaType.TEXT_PLAIN)
public class GreetingResource {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Inject
	private GreetingService service;

	@Path("{country}")
	@GET
	@Timed
	@RolesAllowed("greeting.get")
	public String get(@Auth User user, @PathParam("country") String country) {
		country = country.toUpperCase();
		
		logger.info("Get greeting for {}", country);
		
		Greeting greeting = service.get(country);
		
		if (greeting == null) {
			throw new WebApplicationException(Status.NOT_FOUND);
		}
		
		return user.getName() + " says: " + greeting;
	}
}
