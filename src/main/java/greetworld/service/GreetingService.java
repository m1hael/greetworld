package greetworld.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Provider;

import greetworld.GreetingsCache;
import greetworld.dao.GreetingDao;
import greetworld.data.Greeting;

public class GreetingService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Inject
	private Provider<GreetingDao> daoProvider;
	private GreetingDao dao;
	@Inject
	private GreetingsCache cache;

	public Greeting get(String country) {
		// first check cache
		Greeting greeting = cache.get(country);
		if (greeting == null) {
			logger.debug("Greeting for {} not in cache.", country);

			if (dao == null) {
				dao = daoProvider.get();
			}
			
			// then try database
			greeting = dao.get(country);
			if (greeting == null) {
				logger.debug("Greeting for {} also not in database.", country);
			}
		}
		
		return greeting;
	}
}
