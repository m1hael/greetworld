package greetworld.data;

public class Greeting {

	public String text;
	public String country;
	
	public Greeting() {
		
	}
	
	public Greeting(String text, String country) {
		this.text = text;
		this.country = country;
	}
	
	@Override
	public String toString() {
		return text;
	}
}
