package greetworld.data;

import java.security.Principal;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class User implements Principal {

	private final String username;
	private final Set<String> permissions = new HashSet<>();

	public User(String username) {
		this.username = username;
	}
	
	public User(String username, Collection<String> permissions) {
		this.username = username;
		this.permissions.addAll(permissions);
	}

	@Override
	public String getName() {
		return username;
	}

	public boolean hasPermisson(String permission) {
		return permissions.contains(permission);
	}
	
	@Override
	public String toString() {
		return username;
	}
}
