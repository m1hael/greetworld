package greetworld.job;

import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Provider;

import de.spinscale.dropwizard.jobs.Job;
import de.spinscale.dropwizard.jobs.annotations.On;
import greetworld.GreetingsCache;
import greetworld.dao.GreetingDao;
import greetworld.data.Greeting;

// update cache every minute
@On("0/60 * * * * ?")
public class GreetingsCacheUpdater extends Job {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Inject
	private GreetingsCache cache;
	@Inject
	private Provider<GreetingDao> daoProvider;
	private GreetingDao dao;
	
	@Override
	public void doJob(JobExecutionContext context) throws JobExecutionException {
		logger.info("Updating cache");
		
		if (dao == null) {
			dao = daoProvider.get();
		}
		
		try {
			List<Greeting> greetings = dao.list();
			
			synchronized (cache) {
				cache.clear();
				greetings.forEach(greeting -> cache.put(greeting.country, greeting));
				logger.info("Updated greetings cache with {} entries", greetings.size());
			}
		}
		catch(Exception e) {
			logger.error("Could not update greetings cache. Keeping old data in cache.", e);
		}
	}

}
