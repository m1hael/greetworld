package greetworld.auth;

import greetworld.data.User;
import io.dropwizard.auth.Authorizer;

public class SimplePermissionAuthorizer implements Authorizer<User> {

	@Override
	public boolean authorize(User user, String role) {
		return user.hasPermisson(role);
	}

}
