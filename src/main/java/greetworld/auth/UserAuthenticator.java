package greetworld.auth;

import java.util.Map;
import java.util.Optional;

import greetworld.data.User;
import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.Authenticator;
import io.dropwizard.auth.basic.BasicCredentials;

public class UserAuthenticator implements Authenticator<BasicCredentials, User> {

	private final Map<String, InternalUser> users;
	
	public UserAuthenticator() {
		users = new PropertiesFileUserProfileLoader().loadUserProfiles();
	}

	@Override
	public Optional<User> authenticate(BasicCredentials credentials) throws AuthenticationException {
		InternalUser internalUser = users.get(credentials.getUsername());
		if (internalUser == null) {
			return Optional.empty();
		}
		
		if (!internalUser.password.equals(credentials.getPassword())) {
			return Optional.empty();
		}
		
		return Optional.of(map(internalUser));
	}
	
	private User map(InternalUser internalUser) {
		User user = new User(internalUser.username, internalUser.permissions);
		return user;
	}
}
