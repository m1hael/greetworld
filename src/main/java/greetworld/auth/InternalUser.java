package greetworld.auth;

import java.util.HashSet;
import java.util.Set;

class InternalUser {
	String username;
	String password;
	Set<String> permissions = new HashSet<>();
}
