package greetworld.auth;

import java.util.Arrays;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PropertiesFileUserProfileLoader {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public Map<String, InternalUser> loadUserProfiles() {
		Map<String, InternalUser> users = new HashMap<>();
		loadUsers(users);
		loadRoles(users);
		
		logger.debug("Loaded {} users", users.size());
		
		return users;
	}
	
	private void loadRoles(Map<String, InternalUser> users) {
		Properties properties = new Properties();
		
		try {
			properties.load(this.getClass().getClassLoader().getResourceAsStream("roles.properties"));
			for (Entry<Object, Object> entrySet : properties.entrySet()) {
				InternalUser user = users.get(entrySet.getKey());
				if (user == null) {
					logger.warn("Skipping roles for user " + entrySet.getKey());
				}
				else {
					user.permissions.addAll(Arrays.asList(entrySet.getValue().toString().split(",")));
				}
			}
		}
		catch(Exception e) {
			logger.error("Could not load roles from roles.properties file");
		}
	}

	private void loadUsers(Map<String, InternalUser> users) {
		Decoder decoder = Base64.getDecoder();
		Properties properties = new Properties();
		
		try {
			properties.load(this.getClass().getClassLoader().getResourceAsStream("users.properties"));
			for (Entry<Object, Object> entrySet : properties.entrySet()) {
				InternalUser user = new InternalUser();
				user.username = entrySet.getKey().toString();
				user.password = new String(decoder.decode(entrySet.getValue().toString()));
				users.put(user.username, user);
			}
		}
		catch(Exception e) {
			logger.error("Could not load users from users.properties file");
		}
	}
}
