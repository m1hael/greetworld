package greetworld;

import org.flywaydb.core.Flyway;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;

import com.codahale.metrics.MetricRegistry;
import com.hubspot.dropwizard.guice.GuiceBundle;

import de.spinscale.dropwizard.jobs.JobsBundle;
import greetworld.data.User;
import greetworld.job.GreetingsCacheUpdater;
import greetworld.rs.GreetingResource;
import io.dropwizard.Application;
import io.dropwizard.auth.AuthDynamicFeature;
import io.dropwizard.auth.AuthValueFactoryProvider;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class GreetingApplication extends Application<ApplicationConfiguration> {

	private MetricRegistry metricRegistry;
	private GuiceBundle<ApplicationConfiguration> guice;
	
	public static void main(String[] args) throws Exception {
		new GreetingApplication().run(args);
	}
	
	@Override
	public String getName() {
		return "GreetWorld";
	}
	
	@Override
	public void initialize(Bootstrap<ApplicationConfiguration> bootstrap) {
		metricRegistry = bootstrap.getMetricRegistry();
		
		// setup dependency injection
		guice = GuiceBundle.<ApplicationConfiguration>newBuilder().
		      addModule(new ServiceBinder(metricRegistry)).
		      setConfigClass(ApplicationConfiguration.class).
		      build();
	    bootstrap.addBundle(guice);
	    
	    // register scheduled jobs/tasks
	    bootstrap.addBundle(
	    		new JobsBundle(
	    				guice.getInjector().getInstance(GreetingsCacheUpdater.class))
	    		);
	}
	
	@Override
	public void run(ApplicationConfiguration configuration, Environment env) throws Exception {
		// setup database
		Flyway flyway = guice.getInjector().getInstance(Flyway.class);
		flyway.clean();
        flyway.migrate();
		
        // setup auth
        env.jersey().register(guice.getInjector().getInstance(AuthDynamicFeature.class));
        env.jersey().register(RolesAllowedDynamicFeature.class);
        env.jersey().register(new AuthValueFactoryProvider.Binder<>(User.class));
        
        // register REST resources
        env.jersey().register(guice.getInjector().getInstance(GreetingResource.class));
	}

}
