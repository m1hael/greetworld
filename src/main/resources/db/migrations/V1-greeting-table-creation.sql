CREATE SCHEMA IF NOT EXISTS greetworld;

CREATE TABLE greetworld.greetings (
  country VARCHAR(5) NOT NULL,
  text VARCHAR(255) NOT NULL,
  PRIMARY KEY (country)
);